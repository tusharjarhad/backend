'use strict'
const Router = require('koa-router')

const router =  new Router()

async function hello(ctx) {
    // eslint-disable-next-line fp/no-mutation
    ctx.body = "Hello world"
    return ctx
}

router.get('/hello', hello)

async function hi(ctx) {
    console.log('Headers', ctx.get('Authorization'))
    console.log('Body', ctx.request.body)
    // eslint-disable-next-line fp/no-mutation
    ctx.body = 'Hi'
    return ctx
}

router.post('/hi', hi)

module.exports = router

