/* eslint-disable fp/no-nil */
'use strict'
const Koa = require('koa')
const koaBody = require('koa-body')
const router = require('./router')
const app = new Koa()

const koabody = koaBody({multipart: true})
app.use(koabody)
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)