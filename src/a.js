'use strict'

function sq(b) {
    return b*b
}

module.exports = {
    sq
}