/* eslint-disable fp/no-nil */
'use strict'
const {sq} = require('../src/a')
const { expect } = require('chai')

// eslint-disable-next-line fp/no-let
let assert = require('assert')
describe('Array', function () {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1)
        })
    })

    describe('Square', function() {
        it('should return square ', () => {
            const b = sq(2)
            expect(b).to.eql(4)
        })

        it('Handles negative numbers', () => {
            const b = sq(-2)
            expect(b).to.eql(4)
        })
    })
})